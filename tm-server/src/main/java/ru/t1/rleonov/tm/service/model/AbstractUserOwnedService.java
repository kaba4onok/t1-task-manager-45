package ru.t1.rleonov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.rleonov.tm.api.service.IConnectionService;
import ru.t1.rleonov.tm.api.service.model.IUserOwnedService;
import ru.t1.rleonov.tm.enumerated.Sort;
import ru.t1.rleonov.tm.model.AbstractUserOwnedModel;
import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel,
        R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R>
        implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected String getSortType(@Nullable final Sort sort) {
        if (sort == Sort.BY_CREATED) return "created";
        if (sort == Sort.BY_STATUS) return "status";
        else return "name";
    }

}
